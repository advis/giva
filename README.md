# GIVA

## Business Names - Instance Matching

Instance Matching for Business Names to other data sets such as Food Inspection and third party sources such as Google Places, Yelp, etc

### IDE Settings

Use IntelliJ Idea (or another IDE for ease of build and run). Install latest version Java Development Kit (using 1.8.118 as of this writing). Make sure Maven Integration is enabled.

- In File > Settings:
  - Build > Maven > Runner > JRE: Use JAVA_HOME

- In File > Project Structure:
  - Project SDK: Point to Java version installed on machine: C:\Program Files\Java\jdk1.8.0_x

  - Modules:
    - Source: Language Level 8
    - Dependencies:
      1. < Module Source >
      2. Current java (1.8.x)
      3. derby.jar: JAVA_HOME\db\lib
      4. jgeocoder-0.5-jar-with-dependencies.jar: Giva\InstanceMatching\lib
      5. All Maven Dependencies (downloaded automatically upon build)

### Giva Database

The Giva Database, implemented in Derby, is included as part of the Git repository. It contains:

- Business Model Entity: Generic table to store any entity type.
- Business Licenses
- Food Inspection
- Google Places

** Connection String:** jdbc:derby:/path/to/database
For example: jdbc:derby:D:/City of Chicago/Data/database;create=true

See [Create Table Scripts](documentation/giva_ddl.sql) to create a blank database.
The table layout is set up to mimic the structure of the csv export from the City of Chicago [data portal](http://data.cityofchicago.org/).

### Matcher App Configuration and Run

All settings are configured in config.properties (in the root directory). Please update these properties first. Log Configuration is stored in log4j.properties. This file need not be modified.

Compile the sources and run **advis.main.CityOfChicago**. The source csv files are read and the records are matched. The results are stored to the BUSINESS_MODEL_ENTITY table as well as to the output file specified in the Configuration file.

#### How to include an additional field in the results

1. Add the new field in *BUSINESS_MODEL_ENTITY* table.
2. Add the field (getter) into *advis.dataModel.matcher.IBusinessModel.java* and *UnifiedBusinessModel.java*. If this field name does not match the field name from the sources exactly, add a new getter function in *BusinessLicense.java*, *FoodInspection.java* and *GooglePlaces.java*.
3. In *advis.persistence.DerbyDb.java*, update the insert table script *addSql* to include the new field.
4. In the function *addBusinessModelEntity(...)*, add a new line to set the value for the new field.
5. Recompile and run.

To include source-specific information, consider including a couple of fields called *customField1* and so on and use those instead of forcing all sources to support these fields.

***TODO:*** Print all data from the sources in match results.

### Fetching match results

Run **advis.main.FetchRecords** with the prefix of the business name as the first argument. This will return the results as a json file.
