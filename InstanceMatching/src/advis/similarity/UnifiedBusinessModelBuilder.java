package advis.similarity;

import advis.dataModel.api.AddressFactory;
import advis.dataModel.matcher.*;
import advis.persistence.DerbyDb;
import lombok.Cleanup;
import lombok.val;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;

public class UnifiedBusinessModelBuilder {
    private final double minNameSimilarity;
    private final double minAddressSimilarity;
    private final double maxErrorLatitude;
    private final double maxErrorLongitude;

    private final ISimilarity nameMatcher = new BusinessNameSimilarity();
    private final ISimilarity addressMatcher = new AddressSimilarity();
    private final Alignment alignment = new Alignment();

    private Collection<UnifiedBusinessModel> businessEntities = new ArrayList<>();
    private FastDateFormat timeFormat;
    private String outputFilename, logFilename;

    public UnifiedBusinessModelBuilder(double minNameSimilarity, double minAddressSimilarity, double maxErrorLatitude, double maxErrorLongitude) {
        this.minNameSimilarity = minNameSimilarity;
        this.minAddressSimilarity = minAddressSimilarity;
        this.maxErrorLatitude = maxErrorLatitude;
        this.maxErrorLongitude = maxErrorLongitude;
        timeFormat = FastDateFormat.getDateTimeInstance(FastDateFormat.MEDIUM, FastDateFormat.MEDIUM);
    }

    public void match(String businessLicensesFilename, String foodInspectionFilename, String googlePlacesFilename,
                      String outputFilename, String mismatchedItemsFilename, String logFilename) throws IOException {
        this.outputFilename = outputFilename;
        initLogFile(logFilename);

        @Cleanup BufferedWriter mismatchedItemsFile = Files.newBufferedWriter(Paths.get(mismatchedItemsFilename));

        System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + "Reading business licenses...");
        val businessLicenseEntities = new BusinessEntity<BusinessLicenseFactory.Fields, BusinessLicense>
                (BusinessLicenseFactory.Fields.class, new BusinessLicenseFactory(), businessLicensesFilename);
        val mismatchedItems1 = processBusinessEntities(businessLicenseEntities, true, "business licenses");
        writeMismatchedItems(mismatchedItemsFile, mismatchedItems1);

        System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + "Reading food inspections...");
        val foodInspectionEntities = new BusinessEntity<FoodInspectionFactory.Fields, FoodInspection>
                (FoodInspectionFactory.Fields.class, new FoodInspectionFactory(), foodInspectionFilename);
        val mismatchedItems2 = processBusinessEntities(foodInspectionEntities, false, "food inspections");
        writeMismatchedItems(mismatchedItemsFile, mismatchedItems2);

        System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + "Reading google places...");
        val googlePlacesEntities = new BusinessEntity<GooglePlacesFactory.Fields, GooglePlaces>
                (GooglePlacesFactory.Fields.class, new GooglePlacesFactory(), googlePlacesFilename);
        val mismatchedItems3 = processBusinessEntities(googlePlacesEntities, false, "google places");
        writeMismatchedItems(mismatchedItemsFile, mismatchedItems3);

        saveResults();
        System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + "Done.");
    }

    private void initLogFile(String logFilename) throws IOException {
        this.logFilename = logFilename;
        @Cleanup BufferedWriter logFile = Files.newBufferedWriter(Paths.get(logFilename));
        logFile.write(new Date().toString());
        logFile.newLine();
        logFile.newLine();
    }

    private <Source extends IBusinessModel> void writeMismatchedItems(
            BufferedWriter mismatchedItemsFile, ArrayList<Triple<Source, Double, List<IBusinessModel>>> mismatchedItems)
            throws IOException {
        for (val item : mismatchedItems) {
            val sb = new StringBuilder();
            sb.append(item.getLeft());
            sb.append("\t");
            sb.append(item.getMiddle());
            sb.append("\t");
            if (item.getRight().size() > 3)
                sb.append(item.getRight().size() + " matches");
            else {
                for (val bestMatch : item.getRight()) {
                    sb.append(bestMatch);
                    sb.append("\t");
                }
            }
            sb.append("\n");
            mismatchedItemsFile.write(sb.toString());
        }
    }

    private void saveResults() throws IOException {
        System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + "Writing business entities to file.");
        try {
            @Cleanup BufferedWriter file = Files.newBufferedWriter(Paths.get(this.outputFilename));
            val persistence = new DerbyDb();

            persistence.truncateResults();
            for (val businessEntity : businessEntities) {
                persistence.addBusinessModelEntity(businessEntity);
                file.write(businessEntity.toString());
                file.newLine();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private <Fields extends Enum<Fields>, Source extends IBusinessModel>
    ArrayList<Triple<Source, Double, List<IBusinessModel>>> processBusinessEntities(
            BusinessEntity<Fields, Source> entities, boolean createIfNew, String name) throws IOException {
        int ctr = 0;
        int matched = 0;
        val mismatchedItems = new ArrayList<Triple<Source, Double, List<IBusinessModel>>>();
        while (entities.hasNext()) {
            val businessEntity = entities.next();
            val result = matchBusinessModelToLicenses(businessEntity);

            if (result.getLeft())
                ++matched;
            else {
                if (createIfNew)
                    businessEntities.add(new UnifiedBusinessModel(businessEntity));
                else
                    mismatchedItems.add(Triple.of(businessEntity, result.getMiddle(), result.getRight()));
            }
            if (++ctr % 1000 == 0)
                System.out.print(".");
            if (ctr % 10000 == 0) {
                System.out.printf("%s: %d %s processed. %d matched.%n", timeFormat.format(System.currentTimeMillis()), ctr, name, matched);
                if (createIfNew)
                    System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + businessEntities.size() + " business entities created.");
            }
        }
        System.out.printf("%s: %d %s processed. %d matched.%n", timeFormat.format(System.currentTimeMillis()), ctr, name, matched);
        if (createIfNew)
            System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + businessEntities.size() + " business entities created.");

        return mismatchedItems;
    }

    private <Source extends IBusinessModel> Triple<Boolean, Double, ArrayList<IBusinessModel>> matchBusinessModelToLicenses(
            Source source) throws IOException {
        val matches = new ArrayList<Pair<UnifiedBusinessModel, Mapping>>();
        val bestMatches = new ArrayList<IBusinessModel>();
        double bestSimilarity = 0.0;
        for (val businessEntity : businessEntities) {
            // for (val entity : businessEntity.getEntities()) {
            val similarityScore = matchBusinessModels(source, businessEntity/*entity*/, false);
            val score = similarityScore.getSimilarity();
            if (score == bestSimilarity)
                bestMatches.add(businessEntity);
            if (score > bestSimilarity && score > 0) {
                bestSimilarity = score;
                bestMatches.clear();
                bestMatches.add(businessEntity);
            }

            if (!similarityScore.isMatch()) continue;

            val mapping = alignment.add(source, businessEntity/*entity*/, similarityScore);
            matches.add(Pair.of(businessEntity, mapping));
        }

        if (matches.size() == 0) return Triple.of(false, bestSimilarity, bestMatches);

        if (matches.size() == 1) {
            val entry = matches.get(0);
            entry.getKey().add(entry.getValue(), source);
            return Triple.of(true, bestSimilarity, bestMatches);
        }

        // Multiple matches found - try to find an exact match
        @Cleanup BufferedWriter logFile = Files.newBufferedWriter(Paths.get(logFilename), StandardOpenOption.APPEND);
        logFile.write("Multiple approximate matches found for:");
        logFile.newLine();
        logFile.write(source.toString());
        logFile.newLine();

        val detailedMatches = new ArrayList<SimpleEntry<UnifiedBusinessModel, Mapping>>();
        for (val entry : matches) {
            val similarityScore = matchBusinessModels(source, entry.getKey()/*entity*/, true);
            logFile.write((similarityScore.isMatch() ? "Exact address matched " : "Did not exact address match ") + entry.toString());
            logFile.newLine();
            if (!similarityScore.isMatch()) continue;

            val mapping = alignment.add(source, entry.getKey()/*entity*/, similarityScore);
            detailedMatches.add(new SimpleEntry<>(entry.getKey(), mapping));
        }

        if (detailedMatches.size() == 0) {
            logFile.write("Link unsuccessful.");
            logFile.newLine();
            logFile.newLine();
            return Triple.of(false, bestSimilarity, bestMatches);
        }

        //Found an exact match
        detailedMatches.sort(this::compare);
        detailedMatches.get(0).getKey().add(detailedMatches.get(0).getValue(), source);

        logFile.write("Linked: ");
        logFile.write(detailedMatches.get(0).getValue().toString());
        logFile.newLine();
        logFile.newLine();
        return Triple.of(true, bestSimilarity, bestMatches);
    }

    private int compare(SimpleEntry<UnifiedBusinessModel, Mapping> m1, SimpleEntry<UnifiedBusinessModel, Mapping> m2) {
        double m1Value = m1.getValue().getSimilarityScore().getAddressSimilarity();
        double m2Value = m2.getValue().getSimilarityScore().getAddressSimilarity();

        return m1Value == m2Value ? 0 : m1Value < m2Value ? 1 : -1;
    }

    private <Source extends IBusinessModel, Target extends IBusinessModel> SimilarityScore matchBusinessModels(
            Source source, Target target, boolean forceAddressMatch) throws IOException {
        val cityStateZipSimilarity = target.getCity().toUpperCase().equals(source.getCity().toUpperCase())
                && target.getState().toUpperCase().equals(source.getState().toUpperCase())
                && target.getZipCode().equals(source.getZipCode());
        if (!cityStateZipSimilarity)
            return new SimilarityScore(false, 0, 0, 0);

        boolean isAddressClose = false;
        double addressSimilarity = 0;
        if (source.getLatitude() > 0 && target.getLatitude() > 0 &&
                Math.abs(source.getLatitude() - target.getLatitude()) < maxErrorLatitude &&
                Math.abs(source.getLongitude() - target.getLongitude()) < maxErrorLongitude) {
            isAddressClose = true;
            addressSimilarity = 0.98;
        }
        if (!isAddressClose || forceAddressMatch) {
            val address1 = Arrays.stream(target.getAddresses())
                    .map(street -> AddressFactory.getFullAddress(street, target.getCity(), target.getState(), target.getZipCode()))
                    .toArray(String[]::new);

            val address2 = Arrays.stream(source.getAddresses())
                    .map(street -> AddressFactory.getFullAddress(street, source.getCity(), source.getState(), source.getZipCode()))
                    .toArray(String[]::new);

            addressSimilarity = addressMatcher.getSimilarity(address1, address2);
            if (addressSimilarity <= minAddressSimilarity)
                return new SimilarityScore(false, 0, addressSimilarity, 1);
        }

        val nameSimilarity = nameMatcher.getSimilarity(target.getNames(), source.getNames());
        if (nameSimilarity <= minNameSimilarity)
            return new SimilarityScore(false, nameSimilarity, addressSimilarity, 1);

        return new SimilarityScore(true, nameSimilarity, addressSimilarity, 1);
    }
}
