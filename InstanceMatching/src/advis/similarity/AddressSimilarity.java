package advis.similarity;

import advis.dataModel.api.AddressFactory;
import aml.util.ISub;
import lombok.val;
import net.sourceforge.jgeocoder.AddressComponent;

import java.util.HashMap;
import java.util.Map;

public class AddressSimilarity implements ISimilarity {
    private Map<String, Map<AddressComponent, String>> addressCache = new HashMap<>();

    @Override
    public double getSimilarity(String s1, String s2) {
        val n1 = s1.toUpperCase();
        val n2 = s2.toUpperCase();

        //Check whether the names are equal
        if (n1.equals(n2))
            return 1.0;

        val address1 = getAddressComponents(n1);
        val address2 = getAddressComponents(n2);

        if (address1 == null || address2 == null)
            return ISub.stringSimilarity(n1, n2);

        double similarity = ISub.stringSimilarity(address1.get(AddressComponent.STREET), address2.get(AddressComponent.STREET));
        if (similarity > 0.90 && isEqual(address1, address2, AddressComponent.TYPE) &&
                isEqual(address1, address2, AddressComponent.PREDIR) && isEqual(address1, address2, AddressComponent.CITY) &&
                isEqual(address1, address2, AddressComponent.STATE) && isEqual(address1, address2, AddressComponent.ZIP)) {
            if (isEqual(address1, address2, AddressComponent.NUMBER))
                return similarity * 0.98;
            else
                return similarity * 0.90;
        }
        return 0;
    }

    private boolean isEqual(Map<AddressComponent, String> address1, Map<AddressComponent, String> address2, AddressComponent type) {
        if (!address1.containsKey(type) || !address2.containsKey(type))
            return true;

        val val1 = address1.get(type);
        val val2 = address2.get(type);
        return val1 == null || val2 == null || val1.equals(val2);
    }

    private Map<AddressComponent, String> getAddressComponents(String n1) {
        if (addressCache.containsKey(n1)) {
            return addressCache.get(n1);
        } else {
            // TODO: Use Address parse(String address) instead of Map<AddressComponent, String> parseAddress(...)
            val standardizedAddress = AddressFactory.parseAddress(n1);
            if (standardizedAddress == null)
                return null;

            addressCache.put(n1, standardizedAddress);
            return standardizedAddress;
        }
    }

}
