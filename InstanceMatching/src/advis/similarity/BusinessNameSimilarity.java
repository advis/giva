package advis.similarity;

import aml.util.ISub;
import aml.util.Stemmer;
import aml.util.StopList;
import lombok.val;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.joining;

public class BusinessNameSimilarity implements ISimilarity {
    private final Set<String> stopSet;
    private final Pattern rxStoreNumber;

    public BusinessNameSimilarity() {
        stopSet = StopList.read();
        rxStoreNumber = Pattern.compile("#\\s*\\S+|\\s+\\d+");
    }

    private String getCleanString(String source, boolean removeStopWords) {
        if (removeStopWords)
            return Arrays.stream(source.split("[^a-zA-Z0-9]")).filter(s -> !stopSet.contains(s.toUpperCase())).collect(joining(" "));
        else
            return source.trim();
    }

    private boolean isAcronym(String s1, String s2) {
        return false;

//        String[] s1Parts = s1.split("[^a-zA-Z0-9]+");
//        String[] s2Parts = s2.split("[^a-zA-Z0-9]+");
//
//        if (s1Parts.length > 1 && s2Parts.length > 1) return false;
//
//        char[] shortForm;
//        String[] parts;
//        if (s1Parts.length == 1) {
//            shortForm = s1Parts[0].toLowerCase().toCharArray();
//            parts = s2Parts;
//        } else if (s2Parts.length == 1) {
//            shortForm = s2Parts[0].toCharArray();
//            parts = s1Parts;
//        } else
//            return false;
//
//        if (shortForm.length != parts.length) return false;
//
//        for (int i = 0; i < shortForm.length; ++i)
//            if (shortForm[i] != parts[i].toLowerCase().toCharArray()[0])
//                return false;
//
//        return true;
    }

    private String stem(String source) {
        val stemmer = new Stemmer();
        stemmer.add(source.toCharArray(), source.length());
        stemmer.stem();
        return stemmer.toString();
    }

    @Override
    public double getSimilarity(String n1, String n2) {
        //Check whether the names are equal
        if (n1.equals(n2))
            return 1.0;

        //Remove any store numbers
        n1 = rxStoreNumber.matcher(n1).replaceAll("");
        n2 = rxStoreNumber.matcher(n2).replaceAll("");

        //Compute the String similarity
        double stringSim = ISub.stringSimilarity(n1, n2);

        //Compute the String similarity before removing stop words
        String n1S = getCleanString(n1, false);
        String n2S = getCleanString(n2, false);
        stringSim = Math.max(stringSim, ISub.stringSimilarity(n1S, n2S) * 0.98);

        //Compute the String similarity after removing stop words
        n1S = getCleanString(n1, true);
        n2S = getCleanString(n2, true);
        stringSim = Math.max(stringSim, ISub.stringSimilarity(n1S, n2S) * 0.96);

        //Compute the String similarity after stemming
        if (n1.split(" ").length == 1 && n2.split(" ").length == 1) {
            n1S = stem(n1);
            n2S = stem(n2);
            stringSim = Math.max(stringSim, ISub.stringSimilarity(n1S, n2S) * 0.94);
        }

        if (isAcronym(n1, n2))
            stringSim = Math.max(stringSim, 0.92);

        //Compute the Word similarity (ignoring stop words)
        double wordSim = 0.0;

        //Split the source name into words
        String[] sW = n1.split(" ");
        HashSet<String> n1Words = new HashSet<>();
        for (String s : sW)
            if (!stopSet.contains(s))
                n1Words.add(s);

        String[] tW = n2.split(" ");
        HashSet<String> n2Words = new HashSet<>();
        for (String s : tW)
            if (!stopSet.contains(s))
                n2Words.add(s);

        //Do a weighted Jaccard, where words are weighted by length
        double total = 0.0;
        for (String s : n1Words) {
            if (n2Words.contains(s))
                wordSim += s.length();
            else
                total += s.length();
        }
        for (String s : n2Words)
            total += s.length();
        wordSim /= total;

        //Return the maximum of the string and word similarity
        return Math.max(stringSim, wordSim);
    }
}