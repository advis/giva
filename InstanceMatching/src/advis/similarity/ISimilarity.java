package advis.similarity;

public interface ISimilarity {
    default double getSimilarity(String[] s1, String[] s2) {
        double similarity = 0.0;
        for (String s11 : s1) {
            for (String s22 : s2) {
                similarity = Math.max(similarity, getSimilarity(s11, s22));
                if(similarity == 1.0)
                    return similarity;
            }
        }
        return similarity;
    }

    double getSimilarity(String s1, String s2);
}
