package advis.similarity;

import advis.dataModel.matcher.IBusinessModel;
import lombok.val;

import java.util.ArrayList;
import java.util.Collection;

public class Alignment {
    // This class may be overkill. Considering replacing this class with a simple collection of mappings
    private Collection<Mapping> mappings = new ArrayList<>();

    public Mapping add(IBusinessModel source, IBusinessModel target, SimilarityScore similarityScore) {
        val mapping = new Mapping(source, target, similarityScore);
        mappings.add(mapping);
        return mapping;
    }
}
