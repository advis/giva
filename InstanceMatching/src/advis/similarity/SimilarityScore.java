package advis.similarity;

import lombok.Data;

@Data
public class SimilarityScore {
    private final boolean isMatch;
    private final double nameSimilarity;
    private final double addressSimilarity;
    private final double cityStateZipSimilarity;

    public double getSimilarity() {
        return (nameSimilarity + addressSimilarity + cityStateZipSimilarity) / 3.0;
    }
}
