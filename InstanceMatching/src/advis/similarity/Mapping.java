package advis.similarity;

import advis.dataModel.matcher.IBusinessModel;
import lombok.Data;
import lombok.ToString;

@ToString(exclude = {"TargetEntity"})
@Data()
public class Mapping {
    private final IBusinessModel SourceEntity;
    private final IBusinessModel TargetEntity;
    private final SimilarityScore similarityScore;
}
