package advis.dataModel.api;

import advis.dataModel.matcher.BusinessLicense;
import advis.dataModel.matcher.FoodInspection;
import advis.dataModel.matcher.GooglePlaces;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.val;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static advis.api.Utilities.getMostFrequent;

@Data
@JsonPropertyOrder({"legalName", "doingBusinessAsName", "accountNumber", "address", "metadata", "coordinates"})
public class ApiBusinessModel {
    public List<Metadata> metadata = new ArrayList<>();
    public List<BusinessLicense> businessLicenses = new ArrayList<>();
    public List<FoodInspection> foodInspections = new ArrayList<>();
    public List<GooglePlaces> googlePlaces = new ArrayList<>();

    private String LegalName;
    private String DoingBusinessAsName;
    private String AccountNumber;
    private Address Address;
    private Coordinates coordinates;

    public void updateData() {
        setLegalName(getMostFrequent(businessLicenses.stream().map(BusinessLicense::getLegalName)));
        setDoingBusinessAsName(getMostFrequent(businessLicenses.stream().map(BusinessLicense::getDoingBusinessAsName)));
        setAccountNumber(getMostFrequent(businessLicenses.stream().map(BusinessLicense::getAccountNumber)));

        BusinessLicense mostRecentBusinessLicense = businessLicenses.stream().sorted((a, b) -> b.getStartDate().compareTo(a.getStartDate()))
                .findFirst().orElse(null);
        if (mostRecentBusinessLicense != null) {
            val fullAddress = AddressFactory.getFullAddress(
                    mostRecentBusinessLicense.getAddress(),
                    mostRecentBusinessLicense.getCity(),
                    mostRecentBusinessLicense.getState(),
                    mostRecentBusinessLicense.getZipCode());

            setAddress(AddressFactory.parse(fullAddress));
        }

        //TODO: Look into auto generating Metadatas, somehow
        if (businessLicenses.size() > 0)
            metadata.add(businessLicenses.get(0).getMetadata());
        if (foodInspections.size() > 0)
            metadata.add(foodInspections.get(0).getMetadata());
        if (googlePlaces.size() > 0)
            metadata.add(googlePlaces.get(0).getMetadata());

        coordinates = new Coordinates(
                new Coordinates.LatLon(
                        BigDecimal.valueOf(businessLicenses.stream().mapToDouble(BusinessLicense::getLatitude).average().orElse(0)),
                        BigDecimal.valueOf(businessLicenses.stream().mapToDouble(BusinessLicense::getLongitude).average().orElse(0))),
                new Coordinates.XYCoordinates(0, 0));
    }
}
