package advis.dataModel.api;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({ "sourceName", "sourceUrl", "idProperty", "dateType" })
public class Metadata {
    private final String SourceName;
    private final String SourceUrl;
    private final String IdProperty;
    private final DateType DateType;
    private final String StartDate;
    private final String EndDate;
    private final String ActionDate;

    public enum DateType {None, Range, Exact}
}
