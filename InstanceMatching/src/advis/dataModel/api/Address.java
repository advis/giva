package advis.dataModel.api;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"fullAddress", "streetNumber", "streetDirection", "streetName", "streetSuffix", "city", "state", "zipCode"})
public class Address {
    private final String FullAddress;
    private final String StreetNumber;
    private final String StreetDirection;
    private final String StreetName;
    private final String StreetSuffix;
    private final String City;
    private final String State;
    private final String ZipCode;
}
