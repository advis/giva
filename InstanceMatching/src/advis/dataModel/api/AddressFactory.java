package advis.dataModel.api;

import lombok.val;
import net.sourceforge.jgeocoder.AddressComponent;
import net.sourceforge.jgeocoder.us.AddressParser;
import net.sourceforge.jgeocoder.us.AddressStandardizer;

import java.util.Map;

public class AddressFactory {
    public static Address parse(String address) {
        val standardizedAddress = parseAddress(address);
        return new Address(AddressStandardizer.toSingleLine(standardizedAddress),
                standardizedAddress.get(AddressComponent.NUMBER),
                standardizedAddress.get(AddressComponent.PREDIR),
                standardizedAddress.get(AddressComponent.STREET),
                standardizedAddress.get(AddressComponent.TYPE),
                standardizedAddress.get(AddressComponent.CITY),
                standardizedAddress.get(AddressComponent.STATE),
                standardizedAddress.get(AddressComponent.ZIP));
    }

    public static Map<AddressComponent, String> parseAddress(String address) {
        Map<AddressComponent, String> parsedAddress = AddressParser.parseAddress(address);
        if (parsedAddress == null)
            return null;

        val standardizedAddress = AddressStandardizer.normalizeParsedAddress(parsedAddress);
        if (standardizedAddress == null)
            return null;
        return standardizedAddress;
    }

    public static String getFullAddress(String street, String city, String state, String zipCode) {
        return String.format("%s, %s, %s %s", street, city, state, zipCode);
    }
}
