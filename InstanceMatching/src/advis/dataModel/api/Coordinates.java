package advis.dataModel.api;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonPropertyOrder({ "latLon", "xycoordinates" })
public class Coordinates {
    private final LatLon LatLon;
    private final XYCoordinates XYCoordinates;

    @Data
    public static class LatLon{
        private final BigDecimal Latitude;
        private final BigDecimal Longitude;
    }

    @Data
    public static class XYCoordinates {
        private final double X;
        private final double Y;
    }
}
