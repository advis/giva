package advis.dataModel.matcher;

import advis.api.CsvIterator;
import lombok.val;
import org.apache.commons.lang.time.FastDateFormat;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BusinessEntity<Fields extends Enum<Fields>, Model extends IBusinessModel> implements Iterator<Model> {
    private final Class<Fields> fieldsType; //To workaround the limitations of java
    private final IBusinessModelFactory<Model> modelFactory;
    private final CsvIterator csvIterator;
    private Map<Fields, Integer/*ColumnNumber*/> fieldLocations = new HashMap<>();
    private Model model;
    private int lineCount;

    public BusinessEntity(Class<Fields> fieldsType, IBusinessModelFactory<Model> modelFactory, String filename) throws IOException {
        this.fieldsType = fieldsType;
        this.modelFactory = modelFactory;

        csvIterator = new CsvIterator(filename);
        if (csvIterator.hasNext())
            init(csvIterator.next());
        lineCount = 0;
        getNext();
    }

    private void init(String[] header) {
        int ctr = 0;

        for (String head : header) {
            for (Fields field : fieldsType.getEnumConstants()) {
                if (head.replace("#", "Number").replace(" ", "").toLowerCase().equals(field.name().toLowerCase())) {
                    fieldLocations.put(field, ctr);
                    break;
                }
            }
            ++ctr;
        }
    }

    @Override
    public boolean hasNext() {
        return model != null;
    }

    @Override
    public Model next() {
        val result = model;
        getNext();
        return result;
    }

    private void getNext() {
        model = null;
        while (csvIterator.hasNext()) {
            val data = csvIterator.next();
            ++lineCount;
            if (lineCount % 50000 == 0) {
                val timeFormat = FastDateFormat.getDateTimeInstance(FastDateFormat.MEDIUM, FastDateFormat.MEDIUM);
                System.out.println(timeFormat.format(System.currentTimeMillis()) + ": " + lineCount + " lines read.");
            }

            if (!modelFactory.allowCreate(data))
                continue;

            model = modelFactory.parse(data);
            return;
        }
    }
}
