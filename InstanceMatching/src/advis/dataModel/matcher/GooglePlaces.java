package advis.dataModel.matcher;

import advis.api.Utilities;
import advis.dataModel.api.Metadata;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonIgnoreProperties({"metadata", "names", "addresses", "sourceName"})
@JsonPropertyOrder({"id", "doingBusinessAsName", "address", "city", "state", "zipCode"})
public class GooglePlaces implements IBusinessModel {
    // Only the fields we're interested in, for now.
    private final String Id;
    private final String DoingBusinessAsName;
    private final String Address;
    private final String City;
    private final String State;
    private final String ZipCode;
    private final double Latitude;
    private final double Longitude;

    public static final String SourceName = "googlePlaces";

    @Override
    public String getSourceName() { return SourceName; }

    @Override
    public Metadata getMetadata() {
        return new Metadata(getSourceName(), "data.cityofchicago.org", "id",
                Metadata.DateType.None, "", "", "");
    }

    @Override
    public String[] getNames() {
        return Utilities.nonEmptyStrings(new String[]{getDoingBusinessAsName()});
    }

    @Override
    public String[] getAddresses() {
        return new String[]{getAddress()};
    }
}
