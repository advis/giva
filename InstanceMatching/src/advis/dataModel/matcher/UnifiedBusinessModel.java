package advis.dataModel.matcher;

import advis.api.Utilities;
import advis.dataModel.api.Metadata;
import advis.similarity.Mapping;
import lombok.Getter;
import lombok.ToString;
import lombok.val;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

@ToString(of = {"Entities", "Mappings"})
public class UnifiedBusinessModel implements IBusinessModel {
    @Getter
    private final Collection<IBusinessModel> Entities = new ArrayList<>();
    @Getter
    private final Collection<Mapping> Mappings = new ArrayList<>();

    private final UUID Id;
    @Getter
    private String[] Names;
    @Getter
    private String[] Addresses;
    @Getter
    private String City;
    @Getter
    private String State;
    @Getter
    private String ZipCode;
    @Getter
    private double Latitude;
    @Getter
    private double Longitude;

    public UnifiedBusinessModel(IBusinessModel entity) {
        Entities.add(entity);
        this.Id = UUID.randomUUID();
        updateFields();
    }

    @Override
    public String getId() {
        return Id.toString();
    }

    @Override
    public Metadata getMetadata() {
        return new Metadata(getSourceName(),"data.cityofchicago.org", "id", Metadata.DateType.None,"","","");
    }

    @Override
    public String getSourceName() {
        return "Unified Business Model";
    }

    public void add(Mapping mapping, IBusinessModel entity) {
        Mappings.add(mapping);
        Entities.add(entity);
        updateFields();
    }

    private void updateFields() {
        Names = getEntities().stream().flatMap(bm -> Arrays.stream(bm.getNames())).distinct().toArray(String[]::new);
        Addresses = getEntities().stream().flatMap(bm -> Arrays.stream(bm.getAddresses())).distinct().toArray(String[]::new);
        City = getMostFrequent(IBusinessModel::getCity);
        State = getMostFrequent(IBusinessModel::getState);
        ZipCode = getMostFrequent(IBusinessModel::getZipCode);
        Latitude = getAverage(IBusinessModel::getLatitude);
        Longitude = getAverage(IBusinessModel::getLongitude);
    }

    private String getMostFrequent(Function<IBusinessModel, String> mapper) {
        val valueStream = getEntities().stream().map(mapper);
        return Utilities.getMostFrequent(valueStream);
    }

    private double getAverage(ToDoubleFunction<IBusinessModel> mapper) {
        return getEntities().stream().mapToDouble(mapper).average().orElse(0);
    }
}
