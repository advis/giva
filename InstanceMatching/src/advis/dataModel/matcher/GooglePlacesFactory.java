package advis.dataModel.matcher;

import java.sql.ResultSet;
import java.sql.SQLException;

import static advis.api.Utilities.getDatum;
import static advis.api.Utilities.tryParseLatLong;

public class GooglePlacesFactory implements IBusinessModelFactory<GooglePlaces> {
    @Override
    public GooglePlaces parse(String[] data) {
        return new GooglePlaces(
                getDatum(data, Fields.ID),
                getDatum(data, Fields.DoingBusinessAsName),
                getDatum(data, Fields.Address),
                getDatum(data, Fields.City),
                getDatum(data, Fields.State),
                getDatum(data, Fields.ZipCode),
                tryParseLatLong(getDatum(data, Fields.Latitude)),
                tryParseLatLong(getDatum(data, Fields.Longitude)));
    }

    @Override
    public GooglePlaces parse(ResultSet data) throws SQLException {
        return new GooglePlaces(
                getDatum(data, Fields.ID),
                getDatum(data, Fields.DoingBusinessAsName),
                getDatum(data, Fields.Address),
                getDatum(data, Fields.City),
                getDatum(data, Fields.State),
                getDatum(data, Fields.ZipCode),
                tryParseLatLong(getDatum(data, Fields.Latitude)),
                tryParseLatLong(getDatum(data, Fields.Longitude)));
    }

    @Override
    public boolean allowCreate(String[] data) {
        if (data[Fields.ID.ordinal()].length() == 0) return false;
        return data[Fields.ZipCode.ordinal()].equals("60606");
    }

    public enum Fields {ID, DoingBusinessAsName, Address, City, State, ZipCode, Latitude, Longitude, Rating}
}
