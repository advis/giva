package advis.dataModel.matcher;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IBusinessModelFactory<Model> {
    Model parse(String[] data);

    Model parse(ResultSet data) throws SQLException;

    boolean allowCreate(String[] data);
}
