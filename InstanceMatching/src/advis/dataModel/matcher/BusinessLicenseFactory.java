package advis.dataModel.matcher;

import advis.api.Utilities;
import lombok.val;

import java.sql.ResultSet;
import java.sql.SQLException;

import static advis.api.Utilities.getDatum;
import static advis.api.Utilities.tryParseLatLong;

public class BusinessLicenseFactory implements IBusinessModelFactory<BusinessLicense> {
    @Override
    public BusinessLicense parse(String[] data) {
        val startDate = Utilities.getDate(getDatum(data, Fields.LicenseTermStartDate));
        val expirationDate = Utilities.getDate(getDatum(data, Fields.LicenseTermExpirationDate));
        return new BusinessLicense(
                getDatum(data, Fields.Id),
                getDatum(data, Fields.LicenseId),
                getDatum(data, Fields.LicenseNumber),
                getDatum(data, Fields.LegalName),
                getDatum(data, Fields.DoingBusinessAsName),
                getDatum(data, Fields.AccountNumber),
                getDatum(data, Fields.Address),
                getDatum(data, Fields.City),
                getDatum(data, Fields.State),
                getDatum(data, Fields.ZipCode),
                tryParseLatLong(getDatum(data, Fields.Latitude)),
                tryParseLatLong(getDatum(data, Fields.Longitude)),
                startDate, expirationDate);
    }

    @Override
    public BusinessLicense parse(ResultSet data) throws SQLException {
        val startDate = Utilities.getDate(getDatum(data, Fields.LicenseTermStartDate));
        val expirationDate = Utilities.getDate(getDatum(data, Fields.LicenseTermExpirationDate));
        return new BusinessLicense(
                getDatum(data, Fields.Id),
                getDatum(data, Fields.LicenseId),
                getDatum(data, Fields.LicenseNumber),
                getDatum(data, Fields.LegalName),
                getDatum(data, Fields.DoingBusinessAsName),
                getDatum(data, Fields.AccountNumber),
                getDatum(data, Fields.Address),
                getDatum(data, Fields.City),
                getDatum(data, Fields.State),
                getDatum(data, Fields.ZipCode),
                tryParseLatLong(getDatum(data, Fields.Latitude)),
                tryParseLatLong(getDatum(data, Fields.Longitude)),
                startDate, expirationDate);
    }

    @Override
    public boolean allowCreate(String[] data) {
        if (data[Fields.Id.ordinal()].length() == 0) return false;
        return data[Fields.ZipCode.ordinal()].equals("60606");
    }

    public enum Fields {
        Id, LicenseId, AccountNumber, SiteNumber, LegalName, DoingBusinessAsName, Address, City, State, ZipCode,
        Ward, Precinct, WardPrecinct, PoliceDistrict, LicenseCode, LicenseDescription, BusinessActivityId, BusinessActivity,
        LicenseNumber, ApplicationType, ApplicationCreatedDate, ApplicationRequirementsComplete, PaymentDate,
        ConditionalApproval, LicenseTermStartDate, LicenseTermExpirationDate, LicenseApprovedForIssuance, DateIssued,
        LicenseStatus, LicenseStatusChangeDate, Ssa, Latitude, Longitude, Location
    }
}