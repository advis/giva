package advis.dataModel.matcher;

import advis.dataModel.api.Metadata;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"metadata", "addresses", "sourceName"})
public interface IBusinessModel {
    String getSourceName();
    Metadata getMetadata();
    String getId();
    String[] getNames();
    String[] getAddresses();
    String getCity();
    String getState();
    String getZipCode();
    double getLatitude();
    double getLongitude();
}
