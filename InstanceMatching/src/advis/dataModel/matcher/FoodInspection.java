package advis.dataModel.matcher;

import advis.api.Utilities;
import advis.dataModel.api.Metadata;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.joda.time.DateTime;

@Data
@JsonIgnoreProperties({"foodInspectionDate", "metadata", "names", "addresses", "sourceName"})
@JsonPropertyOrder({"id", "inspectionID", "legalName", "doingBusinessAsName", "address", "city", "state", "zipCode"})
public class FoodInspection implements IBusinessModel {
    // Only the fields we're interested in, for now.
    private final String InspectionID;
    private final String LegalName;
    private final String DoingBusinessAsName;
    private final String Address;
    private final String City;
    private final String State;
    private final String ZipCode;
    private final double Latitude;
    private final double Longitude;
    private final DateTime FoodInspectionDate;

    public static final String SourceName = "foodInspections";

    @Override
    public String getSourceName() { return SourceName; }

    @Override
    public Metadata getMetadata() {
        return new Metadata(getSourceName(), "data.cityofchicago.org", "inspectionID",
                Metadata.DateType.Exact, "", "", "inspectionDate");
    }

    @Override
    public String getId() {
        return getInspectionID();
    }

    @Override
    public String[] getNames() {
        return Utilities.nonEmptyStrings(new String[]{getLegalName(), getDoingBusinessAsName()});
    }

    @Override
    public String[] getAddresses() {
        return new String[]{getAddress()};
    }

    public String getInspectionDate() { return getFoodInspectionDate().toString(); }
}
