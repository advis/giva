package advis.dataModel.matcher;

import advis.api.Utilities;
import lombok.val;

import java.sql.ResultSet;
import java.sql.SQLException;

import static advis.api.Utilities.getDatum;
import static advis.api.Utilities.tryParseLatLong;

public class FoodInspectionFactory implements IBusinessModelFactory<FoodInspection> {
    @Override
    public FoodInspection parse(String[] data) {
        val inspectionDate = Utilities.getDate(getDatum(data, Fields.InspectionDate));
        return new FoodInspection(
                getDatum(data, Fields.InspectionID),
                getDatum(data, Fields.DBAName),
                getDatum(data, Fields.AKAName),
                getDatum(data, Fields.Address),
                getDatum(data, Fields.City),
                getDatum(data, Fields.State),
                getDatum(data, Fields.Zip),
                tryParseLatLong(getDatum(data, Fields.Latitude)),
                tryParseLatLong(getDatum(data, Fields.Longitude)),
                inspectionDate);
    }

    @Override
    public FoodInspection parse(ResultSet data) throws SQLException {
        val inspectionDate = Utilities.getDate(getDatum(data, Fields.InspectionDate));
        return new FoodInspection(
                getDatum(data, Fields.InspectionID),
                getDatum(data, Fields.DBAName),
                getDatum(data, Fields.AKAName),
                getDatum(data, Fields.Address),
                getDatum(data, Fields.City),
                getDatum(data, Fields.State),
                getDatum(data, Fields.Zip),
                tryParseLatLong(getDatum(data, Fields.Latitude)),
                tryParseLatLong(getDatum(data, Fields.Longitude)),
                inspectionDate);
    }

    @Override
    public boolean allowCreate(String[] data) {
        if (data[Fields.InspectionID.ordinal()].length() == 0) return false;
        return data[Fields.Zip.ordinal()].equals("60606");
    }

    public enum Fields {
        InspectionID, DBAName, AKAName, LicenseNumber, FacilityType, Risk, Address, City, State, Zip,
        InspectionDate, InspectionType, Results, Violations, Latitude, Longitude, Location
    }
}
