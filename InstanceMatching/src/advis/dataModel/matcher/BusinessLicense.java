package advis.dataModel.matcher;

import advis.api.Utilities;
import advis.dataModel.api.Metadata;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.joda.time.DateTime;

@Data
@JsonIgnoreProperties({"startDate", "expirationDate", "metadata", "names", "addresses", "sourceName"})
@JsonPropertyOrder({"id", "licenseId", "accountNumber", "legalName", "doingBusinessAsName", "address", "city", "state", "zipCode"})
public class BusinessLicense implements IBusinessModel {
    // Only the fields we're interested in, for now.
    private final String Id;
    private final String LicenseId;
    private final String LicenseNumber;
    private final String LegalName;
    private final String DoingBusinessAsName;
    private final String AccountNumber;
    private final String Address;
    private final String City;
    private final String State;
    private final String ZipCode;
    private final double Latitude;
    private final double Longitude;
    private final DateTime StartDate;
    private final DateTime ExpirationDate;

    public static final String SourceName = "businessLicenses";

    @Override
    public String getSourceName() { return SourceName; }

    @Override
    public Metadata getMetadata() {
        return new Metadata(getSourceName(), "data.cityofchicago.org", "accountNumber",
                Metadata.DateType.Range, "licenseStartDate", "licenseExpirationDate", "");
    }

    @Override
    public String[] getNames() {
        return Utilities.nonEmptyStrings(new String[]{getLegalName(), getDoingBusinessAsName()});
    }

    @Override
    public String[] getAddresses() {
        return new String[]{getAddress()};
    }

    public String getLicenseStartDate() {
        return getStartDate().toString();
    }
    public String getLicenseExpirationDate() {
        return getExpirationDate().toString();
    }
}
