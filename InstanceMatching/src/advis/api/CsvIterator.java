package advis.api;

import com.opencsv.CSVReader;
import lombok.val;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class CsvIterator implements Iterator<String[]> {
    private final CSVReader reader;
    private String[] nextLine;

    public CsvIterator(String filename) throws IOException {
        reader = new CSVReader(new FileReader(filename));
        nextLine = reader.readNext();
    }

    @Override
    public boolean hasNext() {
        return nextLine != null;
    }

    @Override
    public String[] next() {
        val result = nextLine;
        try {
            nextLine = reader.readNext();
        } catch (IOException e) {
            nextLine = null;
        }
        return result;
    }
}
