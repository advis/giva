package advis.api;

import advis.dataModel.api.ApiBusinessModel;
import advis.dataModel.matcher.*;
import advis.persistence.IPersistence;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ApiService {
    private BusinessLicenseFactory businessLicenseFactory = new BusinessLicenseFactory();
    private FoodInspectionFactory foodInspectionFactory = new FoodInspectionFactory();
    private GooglePlacesFactory googlePlacesFactory = new GooglePlacesFactory();
    private IPersistence persistence;

    public ApiService(IPersistence persistence) {
        this.persistence = persistence;
    }

    public String getBusinesses(String prefix) throws JsonProcessingException, SQLException {
        List<ApiBusinessModel> businessResults = new ArrayList<>();

        val businessesByName = persistence.getBusinessesByName(prefix);
        for (val entityId : businessesByName.keySet().stream().sorted().toArray()) {
            val businessInfo = new ApiBusinessModel();
            for (val record : businessesByName.get(entityId)) {
                getModelRecord(businessInfo, record.getKey(), record.getValue());
            }
            businessResults.add(businessInfo);
            businessInfo.updateData();
        }

        val mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        return mapper.writeValueAsString(businessResults);
    }

    private void getModelRecord(ApiBusinessModel businessInfo, String sourceName, String sourceId) throws SQLException {
        ResultSet rs;
        if (sourceName.equals(BusinessLicense.SourceName)) {
            rs = persistence.getModelRow("BUSINESS_LICENSE", "ID", sourceId);
            if (rs.next())
                businessInfo.businessLicenses.add(businessLicenseFactory.parse(rs));
        } else if (sourceName.equals(FoodInspection.SourceName)) {
            rs = persistence.getModelRow("FOOD_INSPECTION", "INSPECTION_ID", sourceId);
            if (rs.next())
                businessInfo.foodInspections.add(foodInspectionFactory.parse(rs));
        } else if (sourceName.equals(GooglePlaces.SourceName)) {
            rs = persistence.getModelRow("GOOGLE_PLACES", "PLACE_ID", sourceId);
            if (rs.next())
                businessInfo.googlePlaces.add(googlePlacesFactory.parse(rs));
        }
    }
}
