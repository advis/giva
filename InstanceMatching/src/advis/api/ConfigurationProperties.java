package advis.api;

import lombok.Cleanup;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationProperties {
    private static final String CONFIG_FILENAME = "config.properties";
    Properties prop = new Properties();

    public ConfigurationProperties() {
        try {
            @Cleanup InputStream input = new FileInputStream(CONFIG_FILENAME);
            prop.load(input);
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public String getString(String key) {
        return prop.getProperty(key);
    }

    public double getDouble(String key) {
        final String value = prop.getProperty(key);
        return Double.parseDouble(value);
    }
}
