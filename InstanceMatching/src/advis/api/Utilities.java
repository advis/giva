package advis.api;

import lombok.val;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utilities {

    static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy");

    public static double tryParseLatLong(final String value) {
        double result;
        try {
            result = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            result = 0.0;
        }
        return result;
    }

    public static <T extends Enum<T>> String getDatum(String[] data, T column) {
        if (data.length > column.ordinal())
            return data[column.ordinal()];
        else
            return "";
    }

    public static <T extends Enum<T>> String getDatum(ResultSet data, T column) {
        try {
            if (data.getMetaData().getColumnCount() >= column.ordinal())
                return data.getString(column.ordinal() + 1);
            else
                return "";
        } catch (SQLException e) {
            return "";
        }
    }

    public static String getMostFrequent(Stream<String> valueStream) {
        val collect = valueStream.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return collect.keySet().stream().sorted(Comparator.comparing(collect::get).reversed()).findFirst().orElse("");
    }

    public static String[] nonEmptyStrings(String[] names) {
        return Arrays.stream(names).filter(n -> n != null && n.length() > 3)
                .map(n -> n.toUpperCase()).toArray(size -> new String[size]);
    }

    public static DateTime getDate(String datum) {
        try {
            return DateTime.parse(datum, dateTimeFormatter);
        }catch (IllegalArgumentException ex){
            return DateTime.now();
        }
    }
}
