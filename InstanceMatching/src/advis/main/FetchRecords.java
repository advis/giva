package advis.main;

import advis.api.ApiService;
import advis.api.ConfigurationProperties;
import advis.persistence.DerbyDb;
import lombok.Cleanup;
import lombok.val;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FetchRecords {

    public static void main(String[] args) throws Exception {
        val properties = new ConfigurationProperties();
        val outputFilename = properties.getString("outputFilename");

        @Cleanup BufferedWriter file = Files.newBufferedWriter(Paths.get(outputFilename));

        val prefix = args.length > 0 ? args[0] : "7-ELEVEN";
        val jsonData = new ApiService(new DerbyDb()).getBusinesses(prefix);
        file.write(jsonData);
        file.close();
    }
}
