package advis.main;

import advis.api.ConfigurationProperties;
import advis.similarity.UnifiedBusinessModelBuilder;
import lombok.val;

public class CityOfChicago {
    public static void main(String[] args) throws Exception {
        val properties = new ConfigurationProperties();
        val baseDirectory = properties.getString("baseDirectory");

        val businessLicensesFilename = baseDirectory + properties.getString("businessLicensesFilename");
        val foodInspectionsFilename = baseDirectory + properties.getString("foodInspectionsFilename");
        val googlePlacesFilename = baseDirectory + properties.getString("googlePlacesFilename");
        val matchResultsFilename = baseDirectory + properties.getString("matchResultsFilename");
        val mismatchedItemsFilename = baseDirectory + properties.getString("mismatchedItemsFilename");
        val logFilename = baseDirectory + properties.getString("multipleMatchesFilename");

        val minNameSimilarity = properties.getDouble("minNameSimilarity");
        val minAddressSimilarity = properties.getDouble("minAddressSimilarity");
        val maxErrorLatitude = properties.getDouble("maxErrorLatitudeMeters") * 0.00000900324936272750000/*meters per degree latitude in Chicago*/;
        val maxErrorLongitude = properties.getDouble("maxErrorLongitudeMeters") * 0.00001204808970911210000/*meters per degree longitude in Chicago*/;

        long time = System.currentTimeMillis();
        System.out.println("Running Instance Matcher");
        new UnifiedBusinessModelBuilder(minNameSimilarity, minAddressSimilarity, maxErrorLatitude, maxErrorLongitude)
                .match(businessLicensesFilename, foodInspectionsFilename, googlePlacesFilename, matchResultsFilename,
                        mismatchedItemsFilename, logFilename);

        time = (System.currentTimeMillis() - time) / 1000;
        System.out.println("Finished in " + time + " seconds");
    }
}
