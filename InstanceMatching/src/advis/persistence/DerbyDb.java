package advis.persistence;

import advis.api.ConfigurationProperties;
import advis.dataModel.matcher.UnifiedBusinessModel;
import lombok.val;

import java.math.BigDecimal;
import java.sql.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DerbyDb implements IPersistence {
    private PreparedStatement stAddBusinessEntity, stGetEntityIdsFromNamePrefix, stGetEntitiesFromId, stTruncateResults;
    private Connection connection;

    public DerbyDb() throws SQLException {
        initConnection();
        prepareStatements();
    }

    private void initConnection() throws SQLException {
        DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
        val dbUrl = new ConfigurationProperties().getString("database");
        connection = DriverManager.getConnection(dbUrl);
    }

    private void prepareStatements() throws SQLException {
        val addSql = "INSERT INTO BUSINESS_MODEL_ENTITY (SOURCE, SOURCE_ID, ENTITY_ID, NAME1, NAME2, STREET1, STREET2, " +
                "CITY, STATE, ZIP_CODE, LATITUDE, LONGITUDE) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        stAddBusinessEntity = connection.prepareStatement(addSql);

        val getEntityIdsSql = "SELECT DISTINCT ENTITY_ID FROM BUSINESS_MODEL_ENTITY WHERE NAME1 LIKE ? ESCAPE '!'";
        stGetEntityIdsFromNamePrefix = connection.prepareStatement(getEntityIdsSql);

        val getEntities = "SELECT SOURCE, SOURCE_ID FROM BUSINESS_MODEL_ENTITY WHERE ENTITY_ID = ?";
        stGetEntitiesFromId = connection.prepareStatement(getEntities);

        val truncateResults = "TRUNCATE TABLE BUSINESS_MODEL_ENTITY";
        stTruncateResults = connection.prepareStatement(truncateResults);
    }

    @Override
    protected void finalize() throws Throwable {
        stAddBusinessEntity.close();
        connection.close();
        super.finalize();
    }

    @Override
    public void addBusinessModelEntity(UnifiedBusinessModel businessEntity) throws SQLException {
        for (val entity : businessEntity.getEntities()) {
            stAddBusinessEntity.setString(1, entity.getSourceName());
            stAddBusinessEntity.setString(2, entity.getId());
            stAddBusinessEntity.setString(3, businessEntity.getId());

            val names = entity.getNames();
            stAddBusinessEntity.setString(4, names.length > 0 ? names[0] : "");
            stAddBusinessEntity.setString(5, names.length > 1 ? names[1] : "");

            val addresses = entity.getAddresses();
            stAddBusinessEntity.setString(6, addresses.length > 0 ? addresses[0] : "");
            stAddBusinessEntity.setString(7, addresses.length > 1 ? addresses[1] : "");
            stAddBusinessEntity.setString(8, entity.getCity());
            stAddBusinessEntity.setString(9, entity.getState());
            stAddBusinessEntity.setString(10, entity.getZipCode());

            stAddBusinessEntity.setBigDecimal(11, BigDecimal.valueOf(entity.getLatitude()));
            stAddBusinessEntity.setBigDecimal(12, BigDecimal.valueOf(entity.getLongitude()));
            stAddBusinessEntity.executeUpdate();
        }
    }

    @Override
    public Map<String/*EntityId*/, List<SimpleEntry<String/*SourceName*/, String/*SourceId*/>>> getBusinessesByName(String namePrefix) throws SQLException {
        namePrefix = namePrefix.replace("!", "!!").replace("%", "!%").replace("_", "!_").replace("[", "![");
        stGetEntityIdsFromNamePrefix.setString(1, namePrefix + "%");
        val entityIds = stGetEntityIdsFromNamePrefix.executeQuery();

        val businesses = new HashMap<String/*EntityId*/, List<SimpleEntry<String/*SourceName*/, String/*SourceId*/>>>();
        while (entityIds.next()) {
            val entityId = entityIds.getString(1);

            stGetEntitiesFromId.setString(1, entityId);
            val entityRecords = stGetEntitiesFromId.executeQuery();

            val records = new ArrayList<SimpleEntry<String/*SourceName*/, String/*SourceId*/>>();
            while (entityRecords.next()) {
                records.add(new SimpleEntry<>(entityRecords.getString(1), entityRecords.getString(2)));
            }
            businesses.put(entityId, records);
        }
        return businesses;
    }

    @Override
    public ResultSet getModelRow(String modelName, String idColumn, String idValue) throws SQLException {
        //TODO: How to parameterize this query?
        val query = String.format("select * from %s where %s = '%s'", modelName, idColumn, idValue);
        return connection.prepareStatement(query).executeQuery();
    }

    @Override
    public void truncateResults() throws SQLException {
        stTruncateResults.execute();
    }
}
