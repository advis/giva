package advis.persistence;

import advis.dataModel.matcher.UnifiedBusinessModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

public interface IPersistence {
    void addBusinessModelEntity(UnifiedBusinessModel businessModel) throws SQLException;
    Map<String/*EntityId*/, List<AbstractMap.SimpleEntry<String/*SourceName*/, String/*SourceId*/>>> getBusinessesByName(String namePrefix) throws SQLException;
    ResultSet getModelRow(String modelName, String idColumn, String idValue) throws SQLException;
    void truncateResults() throws SQLException;
}
